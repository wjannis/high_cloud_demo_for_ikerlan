"""
This script serves as a little demo to show the capabilities of HighCloud net in fast cloud segmentation.
"""
import numpy as np

from utils import CloudDataloader
from backend_onnxruntime import BackendOnnxruntime

def detect_clouds(path2model: str, image_generator: CloudDataloader, plot_seg_map: bool):
    model = BackendOnnxruntime()

    print(f"Using the {model.name()} as inference backend!")
    model.load(path2model)

    t_inf, t_post = 0, 0
    for x, y in image_generator:
        x = model.preprocess(x)
        output_raw, dt_inf = model.predict(np.array([x])[:,:384,:384])
        output, dt_post = model.postprocess(output_raw)
        t_inf += dt_inf
        t_post += dt_post

        # plot last output
        if plot_seg_map:
            import matplotlib.pyplot as plt

            _, axs = plt.subplots(1, 3, figsize=(12, 12))
            axs = axs.flatten()

            imgs = [np.transpose(x[:384,:384,:3], (2, 0, 1)), [y[:384,:384]], [output[0]]]
            titles = ["False color image", "Ground truth", "Prediction"]

            for img, title, ax in zip(imgs, titles, axs):
                ax.imshow(img[0])
                ax.title.set_text(title)

            plt.show()

    print(f"Total inference time for {len(image_generator)} sample(s) was {t_inf + t_post:.1f} ms.")
    print(f"Composed of {t_inf / len(image_generator):.2f} ms inference " \
          f"and {t_post / len(image_generator):.2f} ms postprocessing per sample on average.")


if __name__ == '__main__':
    path2model = "model95/model.onnx"
    path2images = "images"
    path2labels = "labels"

    image_generator = CloudDataloader(path2images, path2labels)

    detect_clouds(path2model, image_generator, True)
