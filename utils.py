import os

import numpy as np
from PIL import Image
#import tensorflow as tf


class CloudDataloader: #(tf.keras.utils.Sequence):
    def __init__(self, path2imgs, path2labels):
        self.x = [os.path.join(path2imgs, f) for f in os.listdir(path2imgs) if not f.startswith('.') and f.endswith('.png')]
        self.y = [os.path.join(path2labels, f) for f in os.listdir(path2labels) if not f.startswith('.') and f.endswith('.png')]
        self.x.sort()
        self.y.sort()

    def __len__(self):
        return len(self.y)

    def __getitem__(self, idx):
        img = np.array(Image.open(self.x[idx])).astype(np.float32)
        gt = np.array(Image.open(self.y[idx])).astype(np.float32)
        return img, gt

from functools import wraps
from time import time, sleep

def measure_time(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        te = time()
        return result, te-ts
    return wrap

# a = CloudDataloader("../data/imgs", "../data/labels")
# print(len(a))
# for x, y in a:
#     print(y)